		
		<section class="footer" style="background-color:#222; padding-bottom: 10px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 pagesFooter" >
				<h3>Pages</h3>
					<ul>
						<?php $menus = wp_get_nav_menus();?>
						<?php
								$menu_items = wp_get_nav_menu_items($menus[0]);        
								foreach ($menu_items as $menu_item):
									$title = $menu_item->title;
									$titlePage =  get_the_title($ID);

									echo "<li>";
										$color = "";
										echo "<a";
											echo " style='margin:5px auto; display:block;'";
											echo " href='$menu_item->url'>";
											echo "$menu_item->title";
										echo "</a>";	
									echo "</li>";
								endforeach;							
											?>	
					</ul>
				</div>
				<div class="col-sm-3 contactFooter" >
				<h3>Contact</h3>
					<div class="row">
						<div style="float:left; padding:0px 10px 0px 15px; margin:0px;">
							<div class="circle" ><p  ><span class="glyphicon glyphicon-envelope"> </span></p></div>
						</div>
						<div style="float:left; padding:5px 5px 0 5px; margin:0px;">
							<p ><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Contact me' ) ) ); ?>">Contact me here!</a></p>
						</div>
					</div>
					
					<div class="row">
						<div style="float:left; padding:4px 10px 0px 15px;">
							<div class="circle" ><p><span class="glyphicon glyphicon-map-marker"> </span></p></div>
						</div>
						<div style="float:left; padding:0px 5px 0 5px;">
							<p >Wooloowin, QLD 4030<br />Brisbane, Australia</p>						
						</div>
					</div>

					<div class="row">
						<div style="float:left; padding:0px 10px 0px 15px;">
							<div class="circle" ><p><span class="glyphicon glyphicon-earphone"></span></p></div>
						</div>
						<div style="float:left; padding:5px 5px 0 5px;">
							<p >+61 415 970 494</p>						
						</div>
					</div>
				</div>
				
				<div class="col-sm-3 followFooter" >
				<h3>Follow</h3>
					<ul>
						<li><a class="icons-social-media1" id="1" target="_blank" href="http://au.linkedin.com/pub/alexis-ribault/27/581/340" ></a></li>
						<li><a class="icons-social-media2" id="2" target="_blank" href="https://twitter.com/RIBAULTAlexis"></a></li>
						<li><a class="icons-social-media3" id="3" target="_blank" href="https://www.facebook.com/pages/Web-design-Web-Programming/221536587950819"></a></li>
						<li><a class="icons-social-media4" id="4" target="_blank" href="https://plus.google.com/113164425917927326627/?rel=author"></a></li>
					</ul>
					<br />
				</div>
				<div class="col-sm-3 aboutMeFooter" >
				<h3>About me</h3>
				<div class="row">
					<div class="col-sm-8">
						<a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a>
						<p >Web Developer &amp; Web Designer</p>	
						</div>
					<div class="col-sm-4">
						<img src="<?php bloginfo('template_directory'); ?>/img/Alexis-Ribault-very-min.jpg" alt="Alexis Ribault" class="img-circle" style="width:60px; display:block; margin: 0 auto;" />  
					</div>					
				</div>
				<p><?php bloginfo('description'); ?></p>

				</div>
			</div>
		</div>
	</section>
	<section style="background-color:#0D0D0D;">
	<p style="text-align:center;color:white;padding:10px;margin:0px;">&copy;<?php echo date("Y"); echo " - "; bloginfo('name'); ?></p>
	</section>

<!-- Don't forget analytics -->
	</div>

	<?php wp_footer(); ?>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	
	<!-- jquery -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    
	<script src="<?php bloginfo('template_directory'); ?>/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/pluginsAndMyJS.js"></script>
	
	<!-- google map customs -->
	
	<?php 
	$page = get_page_by_title( 'Contact me' );
	if ( is_page($page->ID) )
	{ 
		echo '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>';
		echo '<script src="';
		echo bloginfo("template_directory");
		echo '/js/infobox.min.js"></script>';
	}
	
	$page = get_page_by_title( 'home' );
	$page2 = get_page_by_title( 'homePage' );
	if ( is_page($page->ID) || is_page($page2->ID) )
	{ 
		echo "<script src='";
		echo bloginfo('template_directory');
		echo "/js/fetcher.js'></script>";
	}?>
	
		<script>
			$(function() {
 //For all form validations
 var validate={
  init:function(){
   var $this=this;
   // validation will be done at focus out event
   $('#name, #comments').focusout(function() { 
    $this.checkEmpty($(this));
   });
   $('#email').focusout(function() {
    $this.checkEmail($(this));
   });
  },
  //To check wether it is a valid
  isEmail:function(email){
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(email);
  },
  checkEmpty:function($this){
    if (!$this.val())
     $this.addClass('error');
    else
     $this.removeClass('error');
  },
  checkEmail:function($this){
   if (!$this.val() || !this.isEmail($this.val()))
    $this.addClass('error');
   else
    $this.removeClass('error');
  }
 };
 validate.init();
 
 //Ajax submit
 var ajax={
  init:function(){
   $this=this;
   $('#cform').submit(function(e){
    e.preventDefault();
    var action = $(this).attr('action');
    $this.ajaxSubmit($(this),action);
   });
  },
  ajaxSubmit:function($this,action){
   if ($('#contact .error').size()>0) 
    return false;
   $('#submit')
   .after('<img src="<?php bloginfo('template_directory'); ?>/ajax-loader.gif" class="loader" />')
   .attr('disabled','disabled');
 
   $.post(action, $('#cform').serialize(),
    function(data){
     
     if(data.match('success') != null) 
	 {
	 $('#messageFail').hide();
	  $('#message').html('Thank You! We will get back to you soon.');
      $('#message').slideDown();
      $('#cform img.loader').fadeOut('slow',function(){$(this).remove()});
      $('#cform #submit').removeAttr('disabled');
      $('#contactform').slideUp('slow');
	  }
	 else if (data.match('fail') != null)
	 {
	  $('#message').hide();
	  $('#messageFail').html('The captcha is wrong');
	  $('#messageFail').slideDown();
	  $('#cform img.loader').fadeOut('slow',function(){$(this).remove()});
      $('#cform #submit').removeAttr('disabled');
      $('#contactform').slideUp('slow');
	  }
	  
    }
   );
  }
 };
 ajax.init();
 
 
});

</script>

	
			</div><!-- header (container) -->
		</div> <!-- header -->
	</body>
</html>