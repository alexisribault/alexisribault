
	$(document).ready(function() {
 
    /* initialize shuffle plugin */
    var $grid = $('#grid');
 
    $grid.shuffle({
        itemSelector: '.item' // the selector for the items in the grid
    });
	var groupAll = $('#filter a').attr('data-group');
	$grid.shuffle('shuffle', groupAll );
	$(".item").css('visibility', 'visible');
	
	$('#filter a').click(function (e) {
    e.preventDefault();
 
    // set active class
    $('#filter a').removeClass('active');
    $(this).addClass('active');
 
    // get group name from clicked item
    var groupName = $(this).attr('data-group');
 
    // reshuffle grid
    $grid.shuffle('shuffle', groupName );
});
});

	
$("section").waypoint( function( direction ) {
    if( direction === 'down' ) {
        change( $( this ) );
    }
}, { offset: '7%' } ).waypoint( function( direction ) {
    if( direction === 'up' ) {
        change( $( this ) );
    }
}, { offset: '-10%' } );

function change($section){
    $('ul li').removeClass('active');
    currentSection = $section.attr('id');
    $('.target-'+currentSection).addClass('active');
}



	// When the document is loaded...
    $(document).ready(function()
	{
		// Scroll the whole document 
		$('#box-links').localScroll({
		   target:'body',
		   duration:500,
		   offset: -50
		});
	});
	
		// When the document is loaded...
    $(document).ready(function()
	{
		// Scroll the whole document 
		$('#linksWorkPage').localScroll({
		   target:'body',
		   duration:500,
		   offset: -70
		});
		$('#summary').localScroll({
		   target:'body',
		   duration:500,
		   offset: -70
		});
		$('#topDocument').localScroll({
		   target:'body',
		   duration:500,
		   offset: -70
		});
	});

	if($(window).width() >= 800 )
	{

	$(window).scroll(function() {
	
		$('#skills').each(function(){
		var imagePos = $(this).offset().top;

		var topOfWindow = $(window).scrollTop();
			if (imagePos < topOfWindow+500) {
			var delay = setTimeout(function(){
				$('.progress-bar').addClass("slideRight");
			 }, 800)
				$(this).addClass("slideUp");				
			}
		});
		
		$('.fadeInLeftBigJquery').each(function(){
		var imagePos = $(this).offset().top;

		var topOfWindow = $(window).scrollTop();
			if (imagePos < topOfWindow+500) {
				$(this).addClass("slideRight");	
				$(this).css('visibility', 'visible');				
			}
		});
		
		$('.fadeInRightBigJquery').each(function(){
		var imagePos = $(this).offset().top;

		var topOfWindow = $(window).scrollTop();
			if (imagePos < topOfWindow+500) {
				$(this).addClass("slideLeft");		
				$(this).css('visibility', 'visible');
			}
		});
		
		$('.logos').each(function(){
		var imagePos = $(this).offset().top;

		var topOfWindow = $(window).scrollTop();
			if (imagePos < topOfWindow+500) {
				$('.logo').addClass("slideUp");		
				$(this).css('visibility', 'visible');
			}
		});
		
		$('.contactAnimation').each(function(){
		var imagePos = $(this).offset().top;

		var topOfWindow = $(window).scrollTop();
			if (imagePos < topOfWindow+500) {
				$(this).addClass("fadeIn");		
				$(this).css('visibility', 'visible');
			}
		});

	});
	
	$(document).ready(function()
	{
		var delay = setTimeout(function(){
				$('#homeText').addClass("slideDown");
			 }, 250)						
	});

	}else
	{
		$('.logos').css('visibility', 'visible');
		$('.contactAnimation').css('visibility', 'visible');
		$('.fadeInRightBigJquery').css('visibility', 'visible');
		$('.fadeInLeftBigJquery').css('visibility', 'visible');
		$('#skills').css('visibility', 'visible');
		$('.progress-bar').css('visibility', 'visible');
		$('#homeText').css('visibility', 'visible');
	}
	
	