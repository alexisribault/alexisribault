<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PortfolioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $portfolioLastVersion = \App\Portfolio::orderBy('version', 'DESC')->first();

        $version = 1;
        if($portfolioLastVersion) {
            $version = $portfolioLastVersion->version + 1;
        }
        
        $portfolios = \App\Portfolio::orderBy('version', 'DESC')->where('version', $version - 1)->get();

        foreach($portfolios as $portfolio) {
            $portfolio->published = 0;
            $portfolio->save();
        }


        /**
         * Websites
         */

        \App\Portfolio::create(
            [
                'title'        => 'Min Energy – Web development',
                'description'  => 'Work as a web designer in StickyBeakMedia, Brisbane. During this time, I created a website by using HTML and CSS languages from the Photoshop Draft.',
                'project'      => '',
                'technologies' => 'HTML, CSS, Photoshop',
                'link'         => 'http://www.minenergy.com.au',
                'image'        => '/backup/img/min_energy.jpg',
                'type'         => 'website',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2012, 3),
                'ended_at'     => Carbon::create(2012, 3),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'My own website – Web Development & Web Design',
                'description'  => 'Self directed work to produce my own website. The website is using WordPress to manage parts of the content.',
                'project'      => '',
                'technologies' => 'HTML, CSS, Javascript (JQuery), PHP, MySQL Database & WordPress CMS',
                'link'         => 'https://www.alexisribault.com',
                'image'        => '/images/alexisribault-2018.jpg',
                'type'         => 'website',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2012, 3),
                'ended_at'     => Carbon::create(3000),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'University Project – Software Development Project',
                'description'  => 'Creation of a website for a study project during my Master Degree. Three students, including me, were working on the project. The goal of the project was to design and implement a Java based system which controls the storage and retrieval of pallets in a vertical storage warehouse. (Central Queensland University.)',
                'project'      => '',
                'technologies' => 'HTML, CSS, Javascript (JQuery), Ajax, Java EE, Derby Database & Glassfish server.',
                'link'         => '',
                'image'        => '/backup/img/java-project.jpg',
                'type'         => 'website',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2013, 7),
                'ended_at'     => Carbon::create(2013, 10),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Spice Valley of Brisbane',
                'description'  => 'WordPress website fully customized and including a mobile website.',
                'project'      => '',
                'technologies' => 'HTML, CSS, Javascript, PHP, MySQL.',
                'link'         => 'http://spicevalley.com.au/',
                'image'        => '/backup/img/spicevalley.com.au.jpg',
                'type'         => 'website',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2014, 8),
                'ended_at'     => Carbon::create(2014, 8),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'iKeepClient',
                'description'  => 'Amazingly simple online client management software (quoting, invoicing and project tracking).',
                'project'      => 'Create an accounting software where users can access their client list, create quotes and invoices.',
                'technologies' => 'HTML, CSS, PHP, Javascript, JQuery, Bootstrap, MySQL, Laravel, GIT, Angularjs, SEO, Social Media...',
                'link'         => 'http://ikeepclient.com/',
                'image'        => '/backup/img/ikeepclient.com.png',
                'type'         => 'website',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2014, 12),
                'ended_at'     => Carbon::create(2015, 06),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Love-startups',
                'description'  => 'Love-startups selects quality content online, such as videos, quotes and links from the most successful entrepreneurs and most successful companies in order to help you to launch a startup and to stay motivated in the long journey that a startup involve.',
                'project'      => 'Create a WordPress website and improve website cache.',
                'technologies' => 'HTML, CSS, PHP, Javascript, JQuery, Bootstrap, MySQL, GIT, SEO, Social Media…',
                'link'         => 'https://love-startups.com',
                'image'        => '/images/love-startups-2018.jpg',
                'type'         => 'website',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2015, 11),
                'ended_at'     => Carbon::create(3000),
            ]
        );

        /**
         * Flyers
         */
        \App\Portfolio::create(
            [
                'title'        => 'Ice-Skating Event – Telethon event Management',
                'description'  => 'The second flyer listed information about an ice-skating event. For more information please go to the RESUME page.',
                'project'      => '',
                'technologies' => 'Photoshop',
                'link'         => '',
                'image'        => '/backup/img/patinaglace.jpg',
                'type'         => 'flyer',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2009, 04),
                'ended_at'     => Carbon::create(2009, 05),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'The Oceades – Telethon Event Management',
                'description'  => 'To advertise the events for the association “the Telethon”, 2 flyers were created to inform people about the organisation of 2 major events. The First flyer listed a timetable of sporting events held during a night of sport called “the Oceades”. For more information please go to the RESUME page.',
                'project'      => '',
                'technologies' => 'Photoshop',
                'link'         => '',
                'image'        => '/backup/img/flyers2.jpg',
                'type'         => 'flyer',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2009, 04),
                'ended_at'     => Carbon::create(2009, 05),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Ice-Skating Event – Telethon event Management',
                'description'  => 'Creation of a flyer for a study project about the Bobby car Racing. This event was not actually organized. However, to make the project as real as possible, I created a flyer that would be suitable to advertise such an event.',
                'project'      => '',
                'technologies' => 'Photoshop',
                'link'         => '',
                'image'        => '/backup/img/flyer_bobby_race.jpg',
                'type'         => 'flyer',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2011, 10),
                'ended_at'     => Carbon::create(2011, 10),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Work experience as a web developer at Carey IT, Sunshine Coast (Australia).',
                'description'  => 'I learned languages required such as HTML, CSS, PHP and MySQL to create and manage a website. This work experience gave me the opportunity to discover my real interest: information technology.',
                'project'      => '',
                'technologies' => 'HTML, CSS, PHP and MySQL',
                'link'         => 'http://www.careyit.com/',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2011, 11),
                'ended_at'     => Carbon::create(2012, 03),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Web developer at Beyond Web Development, Brisbane Australia',
                'description'  => 'Work experience based on website creation using HTML, CSS, C#.NET and Microsoft SQL server database from Photoshop Drafts.',
                'project'      => '',
                'technologies' => 'HTML, CSS, C#.NET and Microsoft SQL server database',
                'link'         => 'https://www.beyondwebdevelopment.com/',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2012, 11),
                'ended_at'     => Carbon::create(2013, 07),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Co-founder AT Internet Presence, Brisbane Australia',
                'description'  => 'We provide the best professional online presence to our clients, which help you and your business to increase the visibility over the Internet.',
                'project'      => 'customer service, website development, marketing, finance, strategy...',
                'technologies' => 'HTML, CSS, C#.NET and Microsoft SQL server database',
                'link'         => '',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 0,
                'started_at'   => Carbon::create(2012, 11),
                'ended_at'     => Carbon::create(2013, 07),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Co-founder at Fenix, Brisbane Australia',
                'description'  => 'Fenix is an online entertainment streaming service, that’s built from the ground up to eliminate piracy by allowing users to watch their favorite TV shows and movies on any device, for free.',
                'project'      => 'create a platform where user can access free content and provide high quality videos.',
                'technologies' => 'HTML, CSS, PHP, Javascript, JQuery, Bootstrap, MySQL, Laravel, GIT',
                'link'         => '',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2014, 9),
                'ended_at'     => Carbon::create(2015, 01),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Software Engineer & Online Marketing at iKeepClient, Brisbane (Australia)',
                'description'  => 'Amazingly simple online client management software (quoting, invoicing and project tracking).',
                'project'      => 'Create an accounting software where users can access their client list, create quotes and invoices.',
                'technologies' => 'HTML, CSS, PHP (Laravel), Javascript, JQuery, Bootstrap, MySQL, Laravel, GIT, SEO, Social Media...',
                'link'         => 'https://ikeepclient.com',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2014, 12),
                'ended_at'     => Carbon::create(2015, 06),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Full Stack Web Developer at 3rdsense, Sydney Australia',
                'description'  => 'Create apps and games for iPhone, iPad, Android and web. We develop in Flash and HTML5 for Facebook, Mobile, PC and Mac.',
                'project'      => 'Building API for mobile games, work closely with the front end game developer and build CMS to manage these APIs.',
                'technologies' => 'HTML, CSS, PHP, Javascript, JQuery, Bootstrap, MySQL, Laravel, GIT, API (fractual, JWT Auth), Amazon Web Services.',
                'link'         => 'http://3rdsense.com/',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2015, 06),
                'ended_at'     => Carbon::create(2015, 9),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Front End Web Developer at Flight Centre, Brisbane Australia',
                'description'  => 'The airfaire experts and world\'s largest travel agency.',
                'project'      => 'Maintain the booking engine from searching flights to processing payments.',
                'technologies' => 'PHP (Symfony), Backbonejs, Marionettejs, HTML, CSS, Chimpjs functional tests (Selenium, Cucumberjs, Webdriver.io...), VWO (AB testing), Agile Methodology (kanban and standups)...',
                'link'         => 'http://www.flightcentre.com.au/',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2015, 10),
                'ended_at'     => Carbon::create(2016, 03),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Software Engineer / Full Stack Web Developer at Iseekplant, Brisbane Australia',
                'description'  => 'Search for plant hire suppliers. Find local suppliers in areas closest to your project.',
                'project'      => 'Rebuild the entire system from the old system using Laravel framework. The new system is separated into microservices. Those microservices include: a blog, a search engine, a member platform, an online payment system…',
                'technologies' => 'PHP (Laravel), Javascript, jQuery, HTML, CSS, Wordpress, Gulp, Vue Js, Algolia elastic search, Zuora, phpunit, Scrum methodology and Amazon Web Services.',
                'link'         => 'https://iseekplant.com.au',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2016, 03),
                'ended_at'     => Carbon::create(2017, 8),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Senior Software Developer at The University of Queensland, Brisbane Australia',
                'description'  => 'research-intensive institution in the top 50 universities world-wide, offering choice and opportunity in undergraduate and postgraduate learning.',
                'project'      => '1. Build a system that allow casual staff to track their time in order to get paid for the hours they have worked. 2. Rebuild course profiles listing website and simplify the code to improve maintainability and allow to easily add new functionalities.',
                'technologies' => 'PHP (Symfony3), Javascript, BackboneJS, Ember.js, HTML, CSS, phpunit, MySQL, Oracle database, Scrum methodology and Amazon Web Services.',
                'link'         => 'https://uq.edu.au',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2017, 9),
                'ended_at'     => Carbon::create(3000),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Full Stack Web Developer, Brisbane Australia',
                'description'  => 'The Regional Internet Registry administering IP addresses for the Asia Pacific.',
                'project'      => '1. Use zoom API in order to send custom emails to webinar subscribers when they create an account as well as get notified when a webinar they attend is modified or removed. 2. create user friendly react forms to update user profile and settings.',
                'technologies' => 'Javascript, ReactJs, zoom API, Docker and Kubernetes, HTML & CSS , WordPress, custom object oriented PHP',
                'link'         => 'https://www.apnic.net/',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2020,8),
                'ended_at'     => Carbon::create(2020,12),
            ]
        );

       \App\Portfolio::create(
            [
                'title'        => 'In-country Recruitment Database Developer, Brisbane Australia',
                'description'  => 'Pacific Australia Labour Mobility, supporting Pacific workers while they are in Australia',
                'project'      => 'Improve and rebuild part of the tracking system in order record all employees that are 
                                    or will work in Australia. Record Australian company information, for how long the employee will work in 
                                    Australia, all the needed  employee documents, e.g. visa, passport, and other necessary Information. 
                                ',
                'technologies' => 'Javascript, VueJs, Docker, PHP (Laravel), HTML & CSS, Devops (Ubuntu), GitLab deployment',
                'link'         => 'https://thepalladiumgroup.com/',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2021,4),
                'ended_at'     => Carbon::create(2022,2),
            ]
        );

       \App\Portfolio::create(
            [
                'title'        => 'Senior Developer, Brisbane Australia',
                'description'  => 'Entain plc (LSE: ENT) is the FTSE 100 company that is one of the largest sports betting and gaming groups operating in the online and retail sector.',
                'project'      => '1. Use Go lang to build a feature that read the bank document so that it can verify the bank payment records with the one recorded on the admin side 2. Improve several design fea$',
                'technologies' => 'Javascript, VueJs, ReactJs, Go lang, Cypress',
                'link'         => 'https://www.entaingroup.com/',
                'image'        => '',
                'type'         => 'experience',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2022,7),
                'ended_at'     => Carbon::create(2023,2),
            ]
        );

        /* Education */

        \App\Portfolio::create(
            [
                'title'        => 'Higher National Diploma, Business Management and Administration.',
                'description'  => 'Université François Rabelais: France.',
                'project'      => '',
                'technologies' => '',
                'link'         => '',
                'image'        => '',
                'type'         => 'education',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2007, 9),
                'ended_at'     => Carbon::create(2009, 6),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Bachelor of Business minor International Business.',
                'description'  => 'University of Sunshine Coast: Australia.',
                'project'      => '',
                'technologies' => '',
                'link'         => '',
                'image'        => '',
                'type'         => 'education',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2011, 02),
                'ended_at'     => Carbon::create(2011, 11),
            ]
        );

        \App\Portfolio::create(
            [
                'title'        => 'Master of Information Technology, major in Software Development, GPA: 6.167.',
                'description'  => 'Central Queensland University: Australia.',
                'project'      => '',
                'technologies' => '',
                'link'         => '',
                'image'        => '',
                'type'         => 'education',
                'version'      => $version,
                'published'    => 1,
                'started_at'   => Carbon::create(2012, 02),
                'ended_at'     => Carbon::create(2013, 10),
            ]
        );

    }

}
