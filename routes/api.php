<?php

use App\Portfolio;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/portfolio', function () {
    return Portfolio::whereIn('type', ['website', 'flyer'])->where('published', 1)->orderBy('type', 'DESC')->orderBy('id', 'DESC')->get();
});


Route::post('/contact/store', [
    'uses' => 'ContactController@store',
    'as' => 'contact.store'
]);
