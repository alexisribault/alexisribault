<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/portfolio', 'PortfolioController@index')->name('portfolio');
Route::get('/resume', 'ResumeController@index')->name('resume');
Route::get('/contact-me', 'ContactController@index')->name('contact');
Route::get('/privacy-policy', 'PrivacyController@index')->name('privacy');
Route::get('/terms-and-conditions', 'PrivacyController@termsConditions')->name('terms');

Route::get('/sitemap', 'SitemapController@index')->name('sitemap');

Route::post('/word', 'DocumentController@word')->name('word');
Route::post('/pdf', 'DocumentController@pdf')->name('pdf');
