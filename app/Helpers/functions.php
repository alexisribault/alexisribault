<?php

if( ! function_exists('myAsset'))
{
    function myAsset($value)
    {
        if(config('app.https')) {
            return secure_asset($value);
        }
        return asset($value);
    }
}
