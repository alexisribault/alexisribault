<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;

class SitemapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create spatie sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SitemapGenerator::create(url('/'))->getSitemap()
            ->add(url('/about'))
            ->add(url('/portfolio'))
            ->add(url('/resume'))
            ->add(url('/contact-me'))
            ->add(url('/privacy-policy'))
            ->add(url('/terms-and-conditions'))
            ->writeToFile('sitemap.xml');
        $this->info("sitemap created!");
    }
}
