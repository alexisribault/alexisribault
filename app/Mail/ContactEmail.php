<?php

namespace App\Mail;

use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var ContactEmail
     */
    private $contact;

    /**
     * Create a new message instance.
     *
     * @param ContactEmail $contact
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contact@alexisribault.com')
                    ->markdown('emails.contact')
                    ->with([
                        'name' => $this->contact->name,
                        'email' => $this->contact->email,
                        'comment' => $this->contact->comment,
                    ]);
    }
}
