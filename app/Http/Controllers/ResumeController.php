<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;

class ResumeController extends Controller
{
    public function index(){
        $experiences = Portfolio::where('type', 'experience')->where('published', 1)->orderBy('id', 'DESC')->get();
        $educations = Portfolio::where('type', 'education')->where('published', 1)->orderBy('id', 'DESC')->get();
        return view('resume', compact('experiences', 'educations'));
    }
}
