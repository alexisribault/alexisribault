<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DocumentController extends Controller
{
    public function word(){

        $phpWord = $this->document();

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('file/alexis-ribault-resume.docx');

        $temp_file_uri = tempnam('', 'xyz');
        $objWriter->save($temp_file_uri);
        //download code
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=alexis-ribault-resume.docx');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Content-Length: ' . filesize($temp_file_uri));
        readfile($temp_file_uri);
        unlink($temp_file_uri); // deletes the temporary file
        exit;

    }

    public function pdf(){

        $phpWord = $this->document();

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('file/alexis-ribault-resume.docx');

        $file = 'file/alexis-ribault-resume.html';

        // Saving the document as HTML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
        $objWriter->save($file);

        File::append($file, '<style>p {margin: 0px;}</style>');

        file_put_contents($file,str_replace('PHPWord','Alexis Ribault Resume',file_get_contents($file)));


//        \PDF::loadFile(public_path().'/file/alexis-ribault-resume.html')
        \PDF::loadHtml(file_get_contents(public_path().'/file/alexis-ribault-resume.html'))
            ->save('file/alexis-ribault-resume.pdf')
            ->stream('download.pdf');

        $temp_file_uri = 'file/alexis-ribault-resume.pdf';

        //download code
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=alexis-ribault-resume.pdf');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Content-Length: ' . filesize($temp_file_uri));
        readfile($temp_file_uri);
//        unlink($temp_file_uri); // deletes the temporary file
        exit;
    }

    public function document(){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $phpWord->setDefaultFontName('Trebuchet MS');
        $phpWord->setDefaultFontSize(11);

        $bold  = new \PhpOffice\PhpWord\Style\Font();
        $bold->setBold(true);

        // Header
        $section = $phpWord->addSection();
        $section->addText('Alexis Ribault')->setFontStyle($bold);
        $textrun = $section->addTextRun();
        $textrun->addText('E-mail:')->setFontStyle($bold);
        $textrun->addText(' 		alexis.ribault@gmail.com');
        $textrun = $section->addTextRun();

        $textrun->addText('Website:')->setFontStyle($bold);
        $textrun->addText(' 		http://alexisribault.com:');
        $textrun = $section->addTextRun();

        $textrun->addText('Cell phone:')->setFontStyle($bold);
        $textrun->addText(' 		0415 970 494');
        $textrun = $section->addTextRun();

//        $textrun->addText('Address:')->setFontStyle($bold);
//        $textrun->addText(' 		3/89 Albion Rd, Albion 4030, Australia');

        $section->addTextBreak();
        $section->addTextBreak();

        // Education
        $titleSection = $section->addText('Education');
        $titleSection->setParagraphStyle(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER, 'spaceAfter' => 100]);
        $titleSection->setFontStyle(['bold'=>true, 'size'=>13]);


        $section->addTextBreak();
        $section->addTextBreak();

        // Portfolio
        $educations = Portfolio::where('type', 'education')->where('published', 1)->orderBy('id', 'DESC')->get();

        foreach($educations as $education)
        {
            if(\Carbon\Carbon::parse($education->ended_at)->year == 3000)
                $education->ended_at = 'Now';
            else
                $education->ended_at = date('F Y', strtotime($education->ended_at));

            $section->addText(date('F Y', strtotime($education->started_at)) . ' – ' . $education->ended_at);
            $section->addText($education->title)->setFontStyle($bold);
            $section->addText($education->description);

            $section->addTextBreak();

        }

        $section->addTextBreak();

        $titleSection = $section->addText('Professional Experience');
        $titleSection->setParagraphStyle(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER, 'spaceAfter' => 100]);
        $titleSection->setFontStyle(['bold'=>true, 'size'=>13]);

        $section->addTextBreak();

        // Experience
        $experiences = Portfolio::where('type', 'experience')->where('published', 1)->orderBy('id', 'DESC')->get();

        foreach($experiences as $experience)
        {
            if(\Carbon\Carbon::parse($experience->ended_at)->year == 3000)
                $experience->ended_at = 'Now';
            else
                $experience->ended_at = date('F Y', strtotime($experience->ended_at));

            $section->addText(date('F Y', strtotime($experience->started_at)) . ' – ' . $experience->ended_at);
            $section->addText(htmlspecialchars($experience->title))->setFontStyle($bold);

            $textrun = $section->addTextRun();
            $textrun->addText('About: ')->setFontStyle($bold);
            $textrun->addText(htmlspecialchars($experience->description));

            $textrun = $section->addTextRun();
            $textrun->addText('Technologies: ')->setFontStyle($bold);
            $textrun->addText(htmlspecialchars($experience->technologies));

            $textrun = $section->addTextRun();
            $textrun->addText('Project: ')->setFontStyle($bold);
            $textrun->addText(htmlspecialchars($experience->project));

            $section->addTextBreak();

        }

        $section->addTextBreak();

        $titleSection = $section->addText('Skills');
        $titleSection->setParagraphStyle(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER, 'spaceAfter' => 100]);
        $titleSection->setFontStyle(['bold'=>true, 'size'=>13]);
        $section->addTextBreak();

        $section->addText('IT')->setFontStyle($bold);

        $section = $phpWord->addSection([
            'colsNum' => 2,
            'colsSpace' => 1440,
            'breakType' => 'continuous']);

        $section->addText('Programming Languages:')->setFontStyle($bold);
        $section->addText('HTML');
        $section->addText('CSS (Bootstrap)');
        $section->addText('PHP (Laravel, Symfony2, CodeIgniterframework, Wordpress )');
        $section->addText('Javascript (JQuery, VueJS, AngularJS)');
        $section->addText('C#.NET &amp; ASP.NET');
        $section->addText('Java (JSF &amp; JSP)');

        $section->addTextBreak();
        $section->addText('Databases:')->setFontStyle($bold);
        $section->addText('MySQL');
        $section->addText('Derby');
        $section->addText('SQL server');

        $section->addTextBreak();
        $section->addText('Software:')->setFontStyle($bold);
        $section->addText('Windows (XP, VISTA, 7 &amp; 8),');
        $section->addText('Mac OSX, Linux “Ubuntu”,');
        $section->addText('Photoshop, Gimp and Inskape');

        $section->addTextBreak();
        $section->addText('Networking:')->setFontStyle($bold);
        $section->addText('TCP/IP');
        $section->addText('Setup Networks and Servers');
        $section->addText('Setup Websites Hosting');
        $section->addText('AWS (ec2, s3, route53, AIM…)');

        $section = $phpWord->addSection(['breakType' => 'continuous']);

        $section->addTextBreak();
        $section->addText('Languages')->setFontStyle($bold);
        $section->addTextBreak();
        $section->addText('French - Native Language');
        $section->addText('English - Fluent');
        $section->addText('Spanish - Intermediate');

        $section->addTextBreak();
        $section->addTextBreak();

        $titleSection = $section->addText('Activities, interests, abilities');
        $titleSection->setParagraphStyle(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER, 'spaceAfter' => 100]);
        $titleSection->setFontStyle(['bold'=>true, 'size'=>13]);

        $section = $phpWord->addSection([
            'colsNum' => 3,
            'colsSpace' => 720,
            'breakType' => 'continuous']);

        $section->addText('Piano');
        $section->addText('Website');
        $section->addText('Gym');
        $section = $phpWord->addSection(['breakType' => 'continuous']);

        return $phpWord;
    }

}
