<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AboutController extends Controller
{
    public function index(){
        return view('about');
    }
}
