<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index(){
        $websites = Portfolio::where('type', 'website')->where('published', 1)->orderBy('id', 'DESC')->get();
        $flyers = Portfolio::where('type', 'flyer')->where('published', 1)->orderBy('id', 'DESC')->get();
        return view('portfolio', compact('websites', 'flyers'));
    }
}
