<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index(){
        return view('contact');
    }

    public function store(Request $request){

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'email',
            'comment' => 'required',
        ]);

        $data = $request->all();
        $contact = Contact::create($data);

        Mail::to('alexis.ribault@gmail.com')->send(new ContactEmail($contact));

        return $contact;
    }
}
