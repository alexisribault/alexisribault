@extends('layouts.app')

@section('content')

    <div class="jumbotron gradient-background top-margin-for-menu">
        <div class="container">
            <div class="col-md-9">
                <h1 style="font-size: 34px;">Hi, My name is Alexis Ribault</h1>
                <p>I am a Software Engineer and UX/UI Designer.<br></p>
                <p>"This website shows my previous experiences and my interest for software development"</p>
                <p><a class="btn btn-primary" href="/resume" role="button">Resume</a></p>
            </div>
            <div class="col-md-3">
                <br>
                <img src="images/alexisribault-logo.jpeg" alt="Alexis Ribault" class="img-circle" style="width:200px; display:block; margin: 0 auto;">
            </div>
        </div>
    </div>

    <div class="container padding-bottom about-me">

        <h2>About me</h2>

        <div class="row">
            <div class="col-md-4">
                <br>
                <p class="text-center"><i class="fa fa-male" aria-hidden="true"></i></p>
                <h3 class="text-center">Who am I?</h3>
                <p>I am originally from France and I am currently living in Australia. I love challenges and I keep pushing myself to improve everyday.</p>
            </div>
            <div class="col-md-4">
                <br>
                <p class="text-center"><i class="fa fa-magic" aria-hidden="true"></i></p>
                <h3 class="text-center">What I do?</h3>
                <p>I spend most of my time at work and outside of work building new features in order to provide the best information for the users. I care to build high quality applications and would rather spend the extra time to make it perfect.</p>
            </div>
            <div class="col-md-4">
                <br>
                <p class="text-center"><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
                <h3 class="text-center">How I do?</h3>
                <p>Technologies nowadays progress so quickly that us developer need to constantly study to keep up. So, in order to stay competitive, I spend a lot of time learning new skills, essentially about programming but as well about business and marketing.</p>
            </div>
        </div>
    </div>

    <div style="background-color: #efefef; border-top: 1px solid #ddd">
        <div class="container padding-bottom" style="border-top: 1px solid #eee; padding-top: 20px;">

            <h2>Portfolio</h2>

            <tabs></tabs>
        </div>
    </div>
    <div class="jumbotron gradient-background">

        <div class="container">
            <h2>Skills</h2>
            <br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                            <p>PHP (Laravel, Symfony...)</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                            <p>Setup Automated Deployment</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                            <p>Javascript (Node.js, React...)</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                            <p>Tests (phpunit, selenium, Cypress...)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                            <p>HTML / CSS</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                            <p>Mobile Dev (React Native and Cordova)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                            <p>MySQL, Oracle and DynamoDB</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                            <p>Photoshop / Gimp</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
                            <p>Amazon Web Services</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;">
                            <p style="font-size:18px; padding-top:4px;">C#.NET / ASP.NET</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                            <p>Docker</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                            <p style="font-size:18px; padding-top:4px;">Java EE</p>
                        </div>
                    </div>
                </div>
            </div>
            <a href="{{ route('resume') }}" class="btn btn-primary pull-right">See all my skills and experiences &gt;</a>
        </div>

    </div>

    <section class="container padding-bottom">
        <h2>Contact me</h2>
        <br>
        <contact></contact>
    </section>
@endsection
