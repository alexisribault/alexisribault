@component('mail::message')
# A email has been sent to me

You received an email!

name: {{ $name }}<br>
email: {{ $email }}<br>
comment: {{ $comment }}<br>

@php($url = 'this')
@component('mail::button', ['url' => $url])
    View Order
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
