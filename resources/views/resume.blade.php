@extends('layouts.app')

@section('content')

    @include('partials.title', ['title' => 'Resume'])

    <div class="jumbotron gradient-background">
        <div class="container">
            <div class="row">
                <div id="summary" class="col-sm-7">
                    <ul>
                        <li><a href="#" v-scroll-to="'#res-section-1'"><i class="fa fa-graduation-cap"
                                                                          aria-hidden="true"></i> Education</a></li>
                        <li><a href="#" v-scroll-to="'#res-section-2'"><i class="fa fa-briefcase"
                                                                          aria-hidden="true"></i> Professional
                                Experience</a></li>
                        <li><a href="#" v-scroll-to="'#res-section-3'"><i class="fa fa-tasks" aria-hidden="true"></i>
                                Skills</a></li>
                        <li><a href="#" v-scroll-to="'#res-section-4'"><i class="fa fa-picture-o"
                                                                          aria-hidden="true"></i> Activities, Interests,
                                Abilities</a></li>
                    </ul>
                </div>
                <div id="resume-files" class="col-sm-5">
                    <p class="text-center">Please download my resume<br> on WORD or PDF:</p>
                    <form action="word" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="col-xs-4 col-xs-offset-2 document"><i style="font-size: 60px;" class="fa fa-file-word-o" aria-hidden="true"></i></button>
                    </form>
                    <form action="pdf" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="col-xs-4 document"><i style="font-size: 60px;" class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container resume">
        <div class="row">
            <div class="col-md-4">
                <h3 id="res-section-1"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Education</h3>
            </div>

            <div class="col-md-8">
                @foreach($educations as $element)
                    <article>
                        <p><strong>{{ date('F Y', strtotime($element->started_at)) }}
                                - @if(\Carbon\Carbon::parse($element->ended_at)->year == 3000)
                                    Now @else {{ date('F Y', strtotime($element->ended_at)) }}@endif</strong></p>
                        <h4><strong>{{ $element->title }}</strong></h4>
                        <p><strong>About: </strong>{{ $element->description }}<br>
                    </article>
                @endforeach
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-4">
                <h3 id="res-section-2"><i class="fa fa-briefcase" aria-hidden="true"></i> Professional Experience</h3>
            </div>

            <div class="col-md-8">
                @foreach($experiences as $element)
                    <article>
                        <p><strong>{{ date('F Y', strtotime($element->started_at)) }}
                                - @if(\Carbon\Carbon::parse($element->ended_at)->year == 3000)
                                    Now @else {{ date('F Y', strtotime($element->ended_at)) }}@endif</strong></p>
                        <h4><strong>{{ $element->title }}</strong></h4>
                        <p><strong>About: </strong>{{ $element->description }}<br>
                            <strong>Technologies: </strong>{{ $element->technologies }}<br>
                            @if( $element->project != '')
                                <strong>Projects: </strong>{{ $element->project }}<br>
                            @endif
                    </article>
                @endforeach
            </div>
        </div>

        <hr>

        <div class="row skills">
            <div class="col-md-4">
                <h3 id="res-section-3"><i class="fa fa-tasks" aria-hidden="true"></i> Skills</h3>
            </div>
            <div class="col-md-8">
                <h4><em>Information Technology</em></h4>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Software:</strong>
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Microsoft Office</li>
                            <li>Windows (XP, VISTA, 7 &amp; 8)</li>
                            <li>Linux "Ubuntu"</li>
                            <li>Photoshop, Gimp and Inskape</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Networking:</strong>
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>TCP/IP</li>
                            <li>Setup Networks and Servers</li>
                            <li>Setup Websites Hosting</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Programming Languages:</strong>
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>HTML</li>
                            <li>CSS (Bootstrap)</li>
                            <li>PHP ( Symfony2 framework )</li>
                            <li>Java (JSF &amp; JSP)</li>
                            <li>C#.NET &amp; ASP.NET</li>
                            <li>Javascript (JQuery, Bootstrap)</li>
                            <li>SQL (MySQL, derby, SQL server, Datastore (Google Apps Engine))</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Databases:</strong>
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>MySQL</li>
                            <li>Derby</li>
                            <li>SQL server</li>
                            <li>Datastore (Google Apps Engine)</li>
                        </ul>
                    </div>
                </div>

                <div class="languages">
                    <h4><em>Languages</em></h4>
                    <article>
                        <div class="row">
                            <div class="col-md-2">
                                <img alt="french-flag" src="/backup/img/frenchflag.jpg">
                            </div>
                            <div class="col-md-10">
                                <p>French - Native Language</p>
                            </div>
                        </div>
                    </article>

                    <article class="row">
                        <div class="col-md-2">
                            <img alt="australian-flag" src="/backup/img/australiaflag.jpg">
                        </div>
                        <div class="col-md-10">
                            <p>English - Fluent</p>
                        </div>
                    </article>

                    <article class="row">
                        <div class="col-md-2">
                            <img alt="spanish-flag" src="/backup/img/spanishflag.png">
                        </div>
                        <div class="col-md-10">
                            <p>Spanish - Intermediate</p>
                        </div>
                    </article>
                </div>
                <br>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-4">

                <h3 id="res-section-4"><i class="fa fa-picture-o" aria-hidden="true"></i> Activities, Interests,
                    Abilities</h3>
            </div>
            <div class="col-md-8">
                <p><strong>Piano &amp; DJ</strong>: I have been playing the piano since I was 6 years old and have some
                    interest
                    in electronic dance music</p>
                <p><strong>Stock Exchange</strong>: Investment</p>
                <p><strong>Web</strong>: The internet is now in everybody's life.I think it is the upcoming revolution.
                </p>
                <p><strong>Sport</strong>: I usually like to play racket sports such as tennis, badminton and table
                    tennis.</p>
            </div>
        </div>
    </div>
    <br><br>

@endsection
