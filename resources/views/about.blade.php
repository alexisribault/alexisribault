@extends('layouts.app')

@section('content')

    @include('partials.title', ['title' => 'About Me'])

    <div class="container">

        <p>My name is <strong>Alexis Ribault</strong> I have been working in IT for the past 5 years after graduating from a Masters of Information Technology. </p>
        Before having an interest in <strong>software development</strong>, I studied business and graduated with a Bachelor of Business
        I passion for both <strong>Business</strong> and <strong>IT</strong> is still very strong as I believe that those skills are complementary.</p>

        <h2>Software development</h2>
        <p>I have been working as a full stack developer for several years now, with a preference for front end development. I care to build something great and love to do the tasks as perfectly as possible. My experience in the web development space is quite broad as I have built all sorts of websites.

        <div class="row">

            <div class="col-md-6">
                <h3>HTML/CSS</h3>
                Thus, I believe I have a strong knowledge in HTML, CSS, and Javascript as I have been working with those languages for now over 5 years.
                More precisely, I have been using HTML 5 components as well as Sass and Less preprocessors technologies to render css.
                Thus, those experiences help me to create quality layouts with cross-browser compatibility.

                <h3>UX/UI principles</h3>
                I care to build applications that are easy to use for the end user and I have used A/B testing in the past to study the way people interact with the application. That way, it is easy to notice problems that could be found in some parts of the application and fix them.

                <h3>Javascript</h3>
                I have been using a couple of frameworks such as Angular 1 and 2, Vuejs and BackboneJS. I built all sort of applications with those languages including a few one page application as well as a way to replace jQuery scripts.

                <h3>API / Micro services</h3>
                I built APIs and micro services in the past. For example, at 3rd sense we built APIs for mobile video games for android and apple.

            </div>

            <div class="col-md-6">

                <h3>Databases</h3>
                Most of the time, I used using Mysql as a database and in the past I used SQL server. I can write in SQL to search for information in the database and I am able to retrieve information that may require to grab multiple tables.

                <h3>Backend Technologies</h3>
                Finally, I have been working with PHP, Java and .Net over the last few years. I can build application using the object oriented principle and the MVC design pattern.

                <h3>Tests</h3>
                I believe that testing is an important process in the development of an application. In Addition, at Flight Centre I built a lot of automated tests using Selenium, CucumberJs and chimpjs to verify if the application was working correctly. At iSeekplant, we spent the extra time to build unit tests using phpunit in order to test the back end side of the application.

                <h3>Scrum / Agile Methodology</h3>
                I used Jira, bitbucket and confluence for a a few years now. Most of the time I used the Scrum methodology at work including sprints, sprint review and retrospective. Although, I am aware of other methodologies like kanban.
            </div>
        </div>

        <h2>Business</h2>

        <p>Other than creating applications, I am really interested in Business, more specifically in Start-ups and in investing.</p>

        <div class="row">
            <div class="col-md-6">
                <h3>Love Startups</h3>
                <p>I now have been running my blog <a href="https://love-startups.com">love-startups</a> for 3 years, a blog that specifically talks about the struggles when launching a startup. A few years ago, me and my business partner of that time started Ikeepclient. But unfortunately after running out of money we were unable to keep the company running. So, I decided to start a blog about startups to prevent people from making the same mistake we made when we started as entrepreneurs.</p>
            </div>
            <div class="col-md-6">
                <h3>Investing</h3>
                <p>I started investing 10 years ago as I was studying business, at that time, I learned a few trading strategies and how to analyse graphs. Then, I stopped for a little when I moved to Australia. Recently, the passion for investing came back and I decided to study investing deeper by looking into the value investing strategy as well as analysing financial statements.</p>
            </div>
        </div>
    </div>


    <br>


@endsection
