@extends('layouts.app')

@section('content')

    <br><br><br><br><br><br>
{{--    @include('partials.title', ['title' => '404 Page not found'])--}}
<h1 class="text-center" style="font-size: 60px;">404</h1>
    <h2 class="text-center" style="font-size: 45px;">Page not found</h2>
    <br><br>
    <p class="text-center"><a href="{{ route('home') }}" role="button" class="btn btn-primary">Come back to the world</a></p>
    <br><br><br><br><br><br>
@endsection
