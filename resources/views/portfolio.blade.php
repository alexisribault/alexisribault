@extends('layouts.app')

@section('content')

    @include('partials.title', ['title' => 'Portfolio'])


    <div class="gradient-background portfolio-summary">
        <div class="container">
            <div class="col-md-6 target-websites" style="text-align:center;">
                <a href="#" v-scroll-to="'#websites'" class="padding-section">
                    <h4>Websites</h4>
                    <img class="website" src="/backup/img/my-website-2014.jpg">
                </a>
            </div>
            <div class="col-md-6 target-flyers" style="text-align:center;">
                <a href="#" v-scroll-to="'#flyers'" class="padding-section">
                    <h4>Flyers</h4>
                    <img class="website"  alt="" src="/backup/img/flyer.png">
                </a>
            </div>
        </div>
    </div>

    <div class="container">
        <h3 id="websites">Websites</h3>

        @foreach($websites as $element)
            <div class="row">
                <div class="col-sm-8">
                    <p>{{ date('F Y', strtotime($element->started_at)) }} - @if(\Carbon\Carbon::parse($element->ended_at)->year == 3000)Now @else {{ date('F Y', strtotime($element->ended_at)) }}@endif</p>
                    <h4><a href="">{{ $element->title }}</a></h4>
                    <p><strong>About: </strong>{{ $element->description }}</p>
                    <p><strong>Technologies: </strong>{{ $element->technologies }}</p>
                    <p><strong>Link: </strong>@if($element->link)<a href="{{ $element->link }}">{{ $element->link }} @else No link @endif</a></p>
                </div>
                <div class="col-sm-4">
                    <p class="text-center"><img src="{{ $element->image }}" class="website" alt="Love-startups"></p>
                </div>
            </div>
            <hr>
        @endforeach
    </div>

    <div class="container">
        <h3 id="flyers">Flyers</h3>

        @foreach($flyers as $element)
            <div class="row">
                <div class="col-sm-8">
                    <p>{{ date('F Y', strtotime($element->started_at)) }} - @if(\Carbon\Carbon::parse($element->ended_at)->year == 3000)Now @else {{ date('F Y', strtotime($element->ended_at)) }}@endif</p>
                    <h4><a href="">{{ $element->title }}</a></h4>
                    <p><strong>About: </strong>{{ $element->description }}</p>
                    <p><strong>Technologies: </strong>{{ $element->technologies }}</p>
                    <p><strong>Link: </strong>@if($element->link)<a href="{{ $element->link }}">{{ $element->link }} @else No link @endif</a></p>
                </div>
                <div class="col-sm-4">
                    <p class="text-center"><img src="{{ $element->image }}" width="150" class="{{ $element->type }}" alt="Love-startups"></p>
                </div>
            </div>
            <hr>
        @endforeach
    </div>

    <div id="websiteBlog">

    </div>

    <div id="flyersBlog">

    </div>

@endsection
