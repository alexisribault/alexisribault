<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 pages">
                <h4>Pages</h4>
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('about') }}">About me</a></li>
                    <li><a href="{{ route('portfolio') }}">Portfolio</a></li>
                    <li><a href="{{ route('resume') }}">Resume</a></li>
                    {{--<li><a href="{{ route('blog') }}">Blog</a></li>--}}
                    <li><a href="{{ route('contact') }}">Contact me</a></li>
                </ul>
            </div>
            <div class="col-sm-3 contact-footer">
                <h4>Contact</h4>

                <ul>
                    <li><a href="{{ route('contact') }}">
                            <div class="pull-left">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </div>
                            <span class="">Contact me here</span>
                        </a>
                    </li>
                    <li>
                        <div class="pull-left">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <p>
                            Wooloowin, QLD 4030 <br>
                            <span class="next-line">Brisbane, Australia</span>
                        </p>
                    </li>
                    <li>
                        <a href="tel:+61415970494">
                            <div class="pull-left">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                            <span class=""><p>+61 415 970 494</p></span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-3 follow-footer">
                <h4>Follow</h4>
                <ul>
                    <li>
                        <a target="_blank" href="https://www.facebook.com/pages/Web-design-Web-Programming/221536587950819">
                            <div class="pull-left">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </div>
                            <div>Facebook</div>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="http://au.linkedin.com/pub/alexis-ribault/27/581/340">
                            <div class="pull-left">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </div>
                            <span >Linkedin</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://twitter.com/ribaultalexis">
                            <div class="pull-left">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </div>
                            <span >Twitter</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://plus.google.com/113164425917927326627/?rel=author">
                            <div class="pull-left">
                                <i class="fa fa-google" aria-hidden="true"></i>
                            </div>
                            <span>Google +</span>
                        </a>
                    </li>
                </ul>
                <br>
            </div>
            <div class="col-sm-3 aboutMeFooter">
                <h4>About me</h4>
                <div class="row">
                    <div class="col-sm-8">
                        <a href="{{ route('home') }}">Alexis Ribault</a>
                        <p>Software Engineer & UX/UI Designer</p>
                    </div>
                    <div class="col-sm-4">
                        <img src="/images/alexisribault-logo.jpeg"
                             alt="Alexis Ribault" class="img-circle" style="width:60px; display:block; margin: 0 auto;">
                    </div>
                </div>
                <p>"This website shows my previous experiences and my interest for the web design and web
                    development."</p>

            </div>
        </div>
    </div>
</footer>
<section class="bottom-footer">
    <p>©2017 - Alexis Ribault - <a href="{{ route('terms') }}">Terms & conditions</a> - <a href="{{ route('privacy') }}">Privacy policy</a></p>
</section>
