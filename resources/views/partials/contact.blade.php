<section class="padding-bottom">
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
            <h4 class="text-center">Please fill your details below!</h4>
            <br>
            <form>
                <div class="form-group">
                    <label class="control-label" for="name">Name:</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter Your Name">
                </div>
                <div class="form-group">
                    <label class="control-label" for="email">Email:</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter Your Email Address">
                </div>
                <div class="form-group">
                    <label class="control-label" for="comment">Comment:</label>
                    <textarea class="form-control" id="comment" rows="6" placeholder="Enter Your Comment"></textarea>
                </div>
                {{--<div class="form-group">--}}
                {{--<div class="col-sm-offset-2 col-sm-10">--}}
                {{--<div class="checkbox">--}}
                {{--<label><input type="checkbox"> Remember me</label>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
