
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
require('vue');

window.Vue.use(require('vue-resource'));

var VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo, {
  container: "body",
  duration: 500,
  easing: "ease",
  offset: -90,
  onDone: false,
  onCancel: false
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('tabs', require('./components/Tabs.vue'));
Vue.component('contact', require('./components/Contact.vue'));
Vue.component('test', require('./components/Test.vue'));
Vue.component('cookie', require('./components/Cookie.vue'));

const app = new Vue({
    el: '#app'
});
